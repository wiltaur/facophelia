USE [master]
GO

/****** 
Object: Database [FAC_OPHELIA]    
Script Date: 22-Aug-21 10:52:34 AM
Creator: Wilson Salgado
******/

CREATE DATABASE [FAC_OPHELIA]
GO

USE [FAC_OPHELIA]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** 
Object: Table [Cliente]
Description: Tabla para manejar a los clientes y su informaci�n b�sica.
******/
CREATE TABLE [dbo].[Cliente](
	[IdCliente] [nvarchar](20) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Apellido] [nvarchar](50) NOT NULL,
	[Direccion] [nvarchar](50) NULL,
	[FechaNacimiento] [date] NULL,
	[Telefono] [numeric](10, 0) NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)
) ON [PRIMARY]
GO

/****** 
Object: Table [TipoPago]
Description: Tabla para manejar los medios de pago a usar por los clientes.
******/
CREATE TABLE [dbo].[TipoPago](
	[IdPago] [numeric](1, 0) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[MasDetalle] [nvarchar](50) NULL,
 CONSTRAINT [PK_TipoPago] PRIMARY KEY CLUSTERED 
(
	[IdPago] ASC
)
) ON [PRIMARY]
GO

/****** 
Object: Table [ProductoClase]
Description: Tabla para manejar las categor�as de los productos.
******/
CREATE TABLE [dbo].[ProductoClase](
	[IdClase] [numeric](4, 0) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](500) NULL,
 CONSTRAINT [PK_ProductoClase] PRIMARY KEY CLUSTERED 
(
	[IdClase] ASC
)
) ON [PRIMARY]
GO

/****** 
Object: Table [Producto]
Description: Tabla para manejar los productos (
-El precio es el actual del producto, puede cambiar sin afectar las facturas ya creadas. 
-El Inventario es la cantidad en disponibilidad).
******/
CREATE TABLE [dbo].[Producto](
	[IdProducto] [numeric](4, 0) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Precio] [numeric](10, 2) NOT NULL,
	[Inventario] [numeric](18, 0) NOT NULL,
	[IdClase] [numeric](4, 0) NOT NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[IdProducto] ASC
)
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Producto] WITH CHECK ADD CONSTRAINT [FK_Producto_ProductoClase] FOREIGN KEY([IdClase])
REFERENCES [dbo].[ProductoClase] ([IdClase])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_ProductoClase]
GO

/******
Object: Table [Factura]
Description: Tabla para manejar la cabecera de las facturas (
-El n�mero de la factura se crea autom�ticamente de manera autoincremental).
******/
CREATE TABLE [dbo].[Factura](
	[NumFactura] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IdCliente] [nvarchar](20) NOT NULL,
	[IdPago] [numeric](1, 0) NOT NULL,
 CONSTRAINT [PK_Factura] PRIMARY KEY CLUSTERED 
(
	[NumFactura] ASC
)
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Factura] WITH CHECK ADD CONSTRAINT [FK_Factura_Cliente] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_Cliente]
GO

ALTER TABLE [dbo].[Factura] WITH CHECK ADD CONSTRAINT [FK_Factura_TipoPago] FOREIGN KEY([IdPago])
REFERENCES [dbo].[TipoPago] ([IdPago])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_TipoPago]
GO

/****** 
Object: Table [TipoPago]
Description: Tabla para manejar los medios de pago a usar por los clientes(
-Un producto no puede est�r m�s de 1 vez en una factura, para eso se usa el campo de cantidad.
- El precio se pone tambi�n en esta tabla porque es el precio a la fecha que se crea la factura, 
si el producto cambia de precio, la factura no se ve afectada.).
******/
CREATE TABLE [dbo].[FacturaDetalle](
	[NumFacDetalle] [numeric](4, 0) NOT NULL,
	[NumFactura] [numeric](18, 0) NOT NULL,
	[Cantidad] [numeric](4, 0) NOT NULL,
	[PrecioVenta] [numeric](10, 2) NOT NULL,
	[IdProducto] [numeric](4, 0) NOT NULL,
 CONSTRAINT [PK_FacturaDetalle] PRIMARY KEY CLUSTERED 
(
	[NumFacDetalle] ASC,
	[NumFactura] ASC
),
 CONSTRAINT [IX_FacturaDetalle] UNIQUE NONCLUSTERED 
(
	[IdProducto] ASC,
	[NumFactura] ASC
)
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[FacturaDetalle] WITH CHECK ADD CONSTRAINT [FK_FacturaDetalle_Factura] FOREIGN KEY([NumFactura])
REFERENCES [dbo].[Factura] ([NumFactura])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[FacturaDetalle] CHECK CONSTRAINT [FK_FacturaDetalle_Factura]
GO

ALTER TABLE [dbo].[FacturaDetalle] WITH CHECK ADD CONSTRAINT [FK_FacturaDetalle_Producto] FOREIGN KEY([IdProducto])
REFERENCES [dbo].[Producto] ([IdProducto])
GO

ALTER TABLE [dbo].[FacturaDetalle] CHECK CONSTRAINT [FK_FacturaDetalle_Producto]
GO

/****** 
Object: Inserts [Cliente]
Description: Inserci�n de registros para pruebas.
******/
INSERT INTO [dbo].[Cliente]
           ([IdCliente]
           ,[Nombre]
           ,[Apellido]
           ,[FechaNacimiento])
     VALUES
           (1110,
           'Wilson',
           'Salgado',
           '1993-04-24'),
		   (1111,
           'Martha',
           'Blanco',
           '1995-03-23'),
		   (1112,
           'Caro',
           'Pinto',
           '1993-09-28'),
		   (1113,
           'Marlon',
           'Rendon',
           '1991-04-20'),
		   (1114,
           'Michael',
           'Ortega',
           '1999-05-23'),
		   (1115,
           'Viviana',
           'Ruiz',
           '1970-02-21'),
		   (1116,
           'Marcela',
           'Cano',
           '1973-02-22'),
		   (1117,
           'Ivan',
           'Marin',
           '1969-08-17'),
		   (1118,
           'Jeffry',
           'Lule',
           '2000-06-14'),
		   (1119,
           'Kathe',
           'Osorio',
           '1997-01-02')
GO

/****** 
Object: Inserts [TipoPago]
Description: Inserci�n de registros para pruebas.
******/
INSERT INTO [dbo].[TipoPago]
           ([IdPago]
           ,[Nombre])
     VALUES
           (1,
		   'Efectivo'),
		   (2,
		   'Cheque'),
		   (3,
		   'Tarjeta D�bito'),
		   (4,
		   'Tarjeta Cr�dito')
GO

/****** 
Object: Inserts [ProductoClase]
Description: Inserci�n de registros para pruebas.
******/
INSERT INTO [dbo].[ProductoClase]
           ([IdClase]
           ,[Nombre])
     VALUES
           (1,
		   'Frutas'),
		   (2,
		   'Verduras'),
		   (3,
		   'Aseo'),
		   (4,
		   'Granos'),
		   (5,
		   'Bebidas')
GO

/****** 
Object: Inserts [Producto]
Description: Inserci�n de registros para pruebas.
******/
INSERT INTO [dbo].[Producto]
           ([IdProducto]
           ,[Nombre]
           ,[Precio]
           ,[Inventario]
           ,[IdClase])
     VALUES
           (1,
		   'Gaseosa 500ml',
		   2000,
		   100,
		   5),
		   (2,
		   'Gaseosa 1.5l',
		   4000,
		   4,
		   5),
		   (3,
		   'Fr�joles 500ml',
		   1500,
		   3,
		   4),
		   (4,
		   'Lentejas 500ml',
		   1750,
		   50,
		   4),
		   (5,
		   'Papel higi�nico 30m',
		   3000,
		   5,
		   3),
		   (6,
		   'L�mpido 2l',
		   3600,
		   6,
		   3),
		   (7,
		   'Lechuga',
		   550,
		   13,
		   2),
		   (8,
		   'Tomate xlibra',
		   1200,
		   3000,
		   2),
		   (9,
		   'Manzana',
		   650,
		   35,
		   1),
		   (10,
		   'Pera',
		   499,
		   98,
		   1)
GO

/****** 
Object: Inserts [Factura]
Description: Inserci�n de registros para pruebas.
******/
INSERT INTO [dbo].[Factura]
           ([Fecha]
           ,[IdCliente]
           ,[IdPago])
     VALUES
           ('2000-02-20 00:00:00.000',
		   1110,
		   1),
		   ('2000-05-05 00:00:00.000',
		   1110,
		   3),
		   ('2000-08-15 00:00:00.000',
		   1110,
		   1),
		   ('2002-02-02 00:00:00.000',
		   1119,
		   1),
		   ('2001-02-02 00:00:00.000',
		   1117,
		   4),
		   ('2000-04-02 00:00:00.000',
		   1111,
		   2),
		   ('2000-09-02 00:00:00.000',
		   1115,
		   3)
GO

/****** 
Object: Inserts [FacturaDetalle]
Description: Inserci�n de registros para pruebas.
******/
INSERT INTO [dbo].[FacturaDetalle]
           ([NumFacDetalle]
           ,[NumFactura]
           ,[Cantidad]
           ,[PrecioVenta]
           ,[IdProducto])
     VALUES
           (1,
		   1,
		   2,
		   2000,
		   1),		   
		   (2,
		   1,
		   3,
		   4000,
		   2),
		   (1,
		   2,
		   2,
		   2000,
		   1),
		   (2,
		   2,
		   3,
		   4000,
		   2),
		   (1,
		   3,
		   2,
		   1500,
		   3),
		   (2,
		   3,
		   4,
		   1750,
		   4),
		   (1,
		   4,
		   2,
		   3000,
		   5),
		   (1,
		   5,
		   3,
		   3600,
		   6),
		   (1,
		   6,
		   10,
		   550,
		   7),
		   (1,
		   7,
		   10,
		   499,
		   10)
GO