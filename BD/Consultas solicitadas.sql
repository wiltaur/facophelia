--Obtener la lista de precios de todos los productos

SELECT 
P.Nombre AS 'Producto', 
'$ ' + CONVERT(VARCHAR,CAST(P.Precio AS MONEY),1) AS 'Precio' 
FROM Producto AS P 
ORDER BY P.Precio DESC;

--Obtener la lista de productos cuya existencia en el inventario haya llegado al m�nimo permitido (5 unidades)

SELECT 
P.Nombre AS 'Producto', 
P.Inventario AS 'Stock' 
FROM Producto AS P
WHERE P.Inventario <= 5
ORDER BY P.Nombre ASC;

--Obtener una lista de clientes no mayores de 35 a�os que hayan realizado compras entre el 1 de febrero de 2000 
--y el 25 de mayo de 2000

declare @datetimeIni datetime 
set @datetimeIni = '2000-02-01' 

declare @datetimeFin datetime 
set @datetimeFin = '2000-05-25' 

SELECT 
CONCAT(C.Nombre,' ',C.Apellido) AS 'Cliente'
FROM Cliente AS C
INNER JOIN
Factura AS F
ON C.IdCliente = F.IdCliente
WHERE (CAST(DATEDIFF(dd,C.FechaNacimiento,GETDATE()) / 365.25 as int)) <= 35
AND
CONVERT(DATE,F.Fecha) BETWEEN @datetimeIni AND @datetimeFin
GROUP BY C.Nombre, C.Apellido
ORDER BY C.Nombre ASC;

--Obtener el valor total vendido por cada producto en el a�o 2000

SELECT 
P.Nombre AS 'Producto',
'$ ' + CONVERT(VARCHAR,CAST(SUM(FD.Cantidad * FD.PrecioVenta) AS MONEY),1) AS 'Valor total vendido'
FROM FacturaDetalle AS FD
LEFT JOIN Producto AS P
ON P.IdProducto = FD.IdProducto
INNER JOIN
Factura AS F
ON FD.NumFactura = F.NumFactura
WHERE YEAR(F.Fecha) = 2000
GROUP BY P.Nombre, FD.Cantidad, FD.PrecioVenta
Order By FD.PrecioVenta Desc;

--Obtener la �ltima fecha de compra de un cliente 
--y seg�n su frecuencia de compra estimar en qu� fecha podr�a volver a comprar.

SELECT
CONCAT(C.Nombre,' ',C.Apellido) AS 'Cliente',
MAX(F.Fecha) AS '�ltima fecha de compra',
DATEADD(month, DATEDIFF(MONTH,MIN(F.Fecha), MAX(F.Fecha)), MAX(F.Fecha)) AS 'Fecha probable de compra'
FROM Factura AS F
INNER JOIN Cliente AS C
ON F.IdCliente = C.IdCliente
GROUP BY C.NOMBRE, C.Apellido;