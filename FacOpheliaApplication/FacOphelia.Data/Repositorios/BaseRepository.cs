﻿
using FacOphelia.Data.Models;
using FacOphelia.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FacOphelia.Data.Repositorios
{
    /// <summary>
    /// Define las operaciones Basicas a realizar sobre una 
    /// entidad en la base de datos
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected readonly AppDbContext _facOpheliaContext;
        private readonly DbSet<T> _entities;

        public BaseRepository(AppDbContext facOpheliaContext)
        {
            _facOpheliaContext = facOpheliaContext;
            _entities = facOpheliaContext.Set<T>();
        }

        public async Task Add(T entity)
        {
            _entities.Add(entity);
            await _facOpheliaContext.SaveChangesAsync();
        }

        public async Task AddRange(ICollection<T> entity)
        {
            _entities.AddRange(entity);
            await _facOpheliaContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _entities.ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            return await _entities.FindAsync(id);
        }

        public async Task<T> GetById(decimal id)
        {
            return await _entities.FindAsync(id);
        }

        public async Task Update(T entity)
        {
            _entities.Update(entity);
            await _facOpheliaContext.SaveChangesAsync();
        }

        public async Task Delete(T entity)
        {
            _entities.Remove(entity);
            await _facOpheliaContext.SaveChangesAsync();
        }
    }
}
