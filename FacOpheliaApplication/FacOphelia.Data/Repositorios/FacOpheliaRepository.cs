﻿using FacOphelia.Data.Models;
using FacOphelia.Service.DTOs;
using FacOphelia.Service.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacOphelia.Data.Repositorios
{
    public class FacOpheliaRepository : IFacOpheliaRepository
    {
        protected readonly AppDbContext _facOpheliaContext;

        public FacOpheliaRepository(AppDbContext context)
        {
            _facOpheliaContext = context;
        }

        public async Task<List<FacturaResponseDto>> GetFacturas(decimal? numFactura)
        {
            var facturas = await (
                from f in _facOpheliaContext.Facturas
                    where( numFactura == null || f.NumFactura == numFactura)
                    select new FacturaResponseDto
                    {
                        NumFactura = f.NumFactura,
                        Fecha = f.Fecha,
                        FormaPago = f.IdPagoNavigation.Nombre,
                        NombreCliente = f.IdClienteNavigation.Nombre,
                        Detalle = f.FacturaDetalles.Select(fd => new FacturaDetalleDto {
                            Consecutivo = fd.NumFacDetalle,
                            Producto = fd.IdProductoNavigation.Nombre,
                            Cantidad = fd.Cantidad,
                            Precio = fd.PrecioVenta,
                            Subtotal = fd.PrecioVenta * fd.Cantidad
                            }).ToList(),
                        Total = f.FacturaDetalles.Sum(c => c.Cantidad * c.PrecioVenta)
                    }
                ).ToListAsync();

            return facturas;
        }

        private async Task<string> GetNombreTipoPago(decimal idPago)
        {
            string pago = await _facOpheliaContext.TipoPagos
                        .Where(p => p.IdPago == idPago).Select(p => p.Nombre).FirstOrDefaultAsync();
            return pago;
        }

        private async Task<string> GetNombreCliente(string idCliente)
        {
            var cliente = await (from c in _facOpheliaContext.Clientes
                                 where c.IdCliente == idCliente
                                 select new string(c.Nombre + " " + c.Apellido)).FirstOrDefaultAsync();

            return cliente;
        }

        private async Task<List<FacturaDetalleDto>> GetDetalleFactura(decimal idFactura)
        {
            var detalle = await (from f in _facOpheliaContext.FacturaDetalles
                                 where f.NumFactura == idFactura
                                 select new FacturaDetalleDto
                                 {
                                     Consecutivo = f.NumFacDetalle,
                                     Producto = GetNombreProducto(f.IdProducto).Result,
                                     Cantidad = f.Cantidad,
                                     Precio = f.PrecioVenta
                                 }).ToListAsync();

            return detalle;
        }

        private async Task<string> GetNombreProducto(decimal idProducto)
        {
            var producto = await (from p in _facOpheliaContext.Productos
                                  where p.IdProducto == idProducto
                                  select p.Nombre).FirstOrDefaultAsync();

            return producto;
        }

        public async Task<decimal> GetLastNumFactura()
        {
            return await (
                _facOpheliaContext.Facturas.Select(f => f.NumFactura).OrderByDescending(fr => fr).FirstOrDefaultAsync()
                );
        }
    }
}
