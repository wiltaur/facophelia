﻿using FacOphelia.Data.Models;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FacOphelia.Data
{
    public partial class AppDbContext : DbContext
    {
        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<Factura> Facturas { get; set; }
        public virtual DbSet<FacturaDetalle> FacturaDetalles { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<ProductoClase> ProductoClases { get; set; }
        public virtual DbSet<TipoPago> TipoPagos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Factura>(entity =>
            {
                entity.Property(e => e.NumFactura).ValueGeneratedOnAdd();

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.Facturas)
                    .HasForeignKey(d => d.IdCliente);
                    //.HasConstraintName("FK_Factura_Cliente");

                entity.HasOne(d => d.IdPagoNavigation)
                    .WithMany(p => p.Facturas)
                    .HasForeignKey(d => d.IdPago);
                    //.HasConstraintName("FK_Factura_TipoPago");
            });

            modelBuilder.Entity<FacturaDetalle>(entity =>
            {
                entity.HasKey(e => new { e.NumFacDetalle, e.NumFactura });

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.FacturaDetalles)
                    .HasForeignKey(d => d.IdProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull);
                    //.HasConstraintName("FK_FacturaDetalle_Producto");

                entity.HasOne(d => d.NumFacturaNavigation)
                    .WithMany(p => p.FacturaDetalles)
                    .HasForeignKey(d => d.NumFactura);
                    //.HasConstraintName("FK_FacturaDetalle_Factura");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasOne(d => d.IdClaseNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdClase);
                    //.HasConstraintName("FK_Producto_ProductoClase");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
