﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FacOphelia.Data.Models;
using FacOphelia.Service.DTOs;
using FacOphelia.Service.Interfaces;
using FacOpheliaApplication.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FacOpheliaApplication.Controllers
{
    /// <summary>
    /// Gestiona las Peticiones HTML relacionadas con el sistema de facturación.
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class FacOpheliaController : ControllerBase
    {
        private const string ERROR_GUARDAR_INFORMACION = "Error al guardar la informacion.";
        private const string ERROR_CONSULTAR_INFORMACION = "Error al consultar la informacion.";
        private const string ERROR_ELIMINAR_INFORMACION = "Error al eliminar la informacion.";
        private const string ERROR_MODIFICAR_INFORMACION = "Error al modificar la informacion.";
        private readonly IFacOpheliaService _facOpheliaService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="facOpheliaService"></param>
        public FacOpheliaController(IFacOpheliaService facOpheliaService)
        {
            _facOpheliaService = facOpheliaService;
        }

        /// <summary>
        /// Servicio web  que permite la consulta de una Factura filtrada o todas las facturas del sistema.
        /// </summary>
        /// <param name="numFactura"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("~/api/FacOphelia/GetFacturas", Name = "GetFacturas")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponse<IEnumerable<FacturaResponseDto>>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(ApiResponse<bool>))]
        public async Task<IActionResult> GetFacturas(decimal? numFactura)
        {
            try
            {
                var facturas = await _facOpheliaService.GetFacturas(numFactura);
                var response = new ApiResponse<IEnumerable<FacturaResponseDto>>(facturas)
                {
                    IsSuccess = true,
                    ReturnMessage = "Consulta exitosa"
                };
                return Ok(response);
            }
            catch (System.Exception ex)
            {
                var message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                var badResponse = new ApiResponse<string>(message);
                badResponse.IsSuccess = false;
                badResponse.ReturnMessage = ERROR_CONSULTAR_INFORMACION;
                return BadRequest(badResponse);
            }
        }

        /// <summary>
        /// Servicio web que permite la creación de una Factura con el detalle en la BD.
        /// </summary>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponse<FacturaCreaResponseDto>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(ApiResponse<bool>))]
        public async Task<IActionResult> AddFactura(FacturaRequestDto factura)
        {
            try
            {
                var numFactura = await _facOpheliaService.AddFactura(factura);

                var response = new ApiResponse<FacturaCreaResponseDto>(numFactura)
                {
                    IsSuccess = true,
                    ReturnMessage = $"La información ha sido almacenada exitosamente"
                };

                return Ok(response);

            }
            catch (System.Exception ex)
            {
                var message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                var badResponse = new ApiResponse<string>(message);
                badResponse.IsSuccess = false;
                badResponse.ReturnMessage = ERROR_GUARDAR_INFORMACION;
                return BadRequest(badResponse);
            }
        }

        /// <summary>
        /// Servicio web que permite la eliminación de una Factura con el detalle en la BD.
        /// </summary>
        /// <returns></returns>
        [HttpDelete("[action]")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponse<FacturaEliminaResponseDto>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(ApiResponse<bool>))]
        public async Task<IActionResult> DeleteFactura([Required] decimal numFactura)
        {
            try
            {
                var numFacturaResp = await _facOpheliaService.DeleteFactura(numFactura);

                var response = new ApiResponse<FacturaEliminaResponseDto>(numFacturaResp)
                {
                    IsSuccess = true,
                    ReturnMessage = $"La factura ha sido eliminada exitosamente"
                };

                return Ok(response);

            }
            catch (System.Exception ex)
            {
                var message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                var badResponse = new ApiResponse<string>(message);
                badResponse.IsSuccess = false;
                badResponse.ReturnMessage = ERROR_ELIMINAR_INFORMACION;
                return BadRequest(badResponse);
            }
        }

        /// <summary>
        /// Servicio web que permite la modificación de una lista de productos en BD.
        /// </summary>
        /// <returns></returns>
        [HttpPut("[action]")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponse<ProductoResponseDto>))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(ApiResponse<bool>))]
        public async Task<IActionResult> UpdateProducto(ICollection<ProductoRequestDto> productos)
        {
            try
            {
                var productosResp = await _facOpheliaService.UpdateProductos(productos);

                var response = new ApiResponse<IEnumerable<ProductoResponseDto>>(productosResp)
                {
                    IsSuccess = true,
                    ReturnMessage = $"Los productos han sido modificados exitosamente."
                };

                return Ok(response);

            }
            catch (System.Exception ex)
            {
                var message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                var badResponse = new ApiResponse<string>(message);
                badResponse.IsSuccess = false;
                badResponse.ReturnMessage = ERROR_MODIFICAR_INFORMACION;
                return BadRequest(badResponse);
            }
        }
    }
}