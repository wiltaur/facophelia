﻿using AutoFixture;
using FacOphelia.Data;
using FacOphelia.Data.Models;
using FacOphelia.Service.DTOs;
using FacOphelia.Service.Interfaces;
using FacOphelia.Service.Servicios;
using FacOphelia.UnitTest.MockDbContext;
using FacOpheliaApplication.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FacOphelia.UnitTest.TestController
{
    public class TestFacOpheliaController : AppDbContextMock
    {

        private FacOpheliaController _controller;
        private AppDbContext contexto;
        private IFacOpheliaService _service;
        private readonly Mock<IFacOpheliaRepository> _facOpheliaRepository;
        private readonly Mock<IBaseRepository<Factura>> _repFactura;
        private readonly Mock<IBaseRepository<Producto>> _repProducto;

        public TestFacOpheliaController()
        {
            _repFactura = new Mock<IBaseRepository<Factura>>();
            _repProducto = new Mock<IBaseRepository<Producto>>();
            _facOpheliaRepository = new Mock<IFacOpheliaRepository>();
            var nombreDB = Guid.NewGuid().ToString();
            contexto = ConstruirContext(nombreDB);
            Fixture fixture = new Fixture();
            fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(b => fixture.Behaviors.Remove(b));
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            ProductoClase productoClase = fixture.Create<ProductoClase>();
            Producto producto = fixture.Create<Producto>();
            Cliente cliente = fixture.Create<Cliente>();
            TipoPago tipoPago = fixture.Create<TipoPago>();
            Factura factura = fixture.Create<Factura>();

            contexto.Add(productoClase);
            contexto.Add(producto);
            contexto.Add(cliente);
            contexto.Add(tipoPago);
            contexto.Add(factura);

            configurarController();
        }

        void configurarController()
        {
            var httpContext = new DefaultHttpContext();
            _service = new FacOpheliaService(_repFactura.Object, _repProducto.Object, _facOpheliaRepository.Object);
            _controller = new FacOpheliaController(_service)
            {
                ControllerContext = new ControllerContext()
                {
                    HttpContext = httpContext
                }
            };
        }

        [Fact]
        public void AddFacturaOk()
        {
            //Arrange
            Fixture fixture = new Fixture();
            var factura = fixture.Create<FacturaRequestDto>();
            //Act
            var okResult = _controller.AddFactura(factura);
            //Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public async Task AddFacturaException()
        {
            //Arrange
            var mockServicio = new Mock<IFacOpheliaService>();
            Fixture fixture = new Fixture();
            var factura = fixture.Create<FacturaRequestDto>();

            mockServicio.Setup(m => m.AddFactura(It.IsAny<FacturaRequestDto>())).Throws(new Exception());
            _controller = new FacOpheliaController(mockServicio.Object);

            //Act
            var okResult = await _controller.AddFactura(factura);

            //Assert
            Assert.IsType<BadRequestObjectResult>(okResult);
        }


        [Fact]
        public void GetFacturaOk()
        {
            //Act
            var okResult = _controller.GetFacturas(contexto.Facturas.Select(f => f.NumFactura).FirstOrDefault());
            //Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public async Task GetFacturaException()
        {
            //Arrange
            var mockServicio = new Mock<IFacOpheliaService>();

            mockServicio.Setup(m => m.GetFacturas(It.IsAny<decimal>())).Throws(new Exception());
            _controller = new FacOpheliaController(mockServicio.Object);

            //Act
            var okResult = await _controller.GetFacturas(contexto.Facturas.Select(f => f.NumFactura).FirstOrDefault());

            //Assert
            Assert.IsType<BadRequestObjectResult>(okResult);
        }

        [Fact]
        public void DeleteFacturaOk()
        {
            //Act
            var okResult = _controller.DeleteFactura(contexto.Facturas.Select(f => f.NumFactura).FirstOrDefault());
            //Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public async Task DeleteFacturaException()
        {
            //Arrange
            var mockServicio = new Mock<IFacOpheliaService>();

            mockServicio.Setup(m => m.DeleteFactura(It.IsAny<decimal>())).Throws(new Exception());
            _controller = new FacOpheliaController(mockServicio.Object);

            //Act
            var okResult = await _controller.DeleteFactura(contexto.Facturas.Select(f => f.NumFactura).FirstOrDefault());

            //Assert
            Assert.IsType<BadRequestObjectResult>(okResult);
        }

        [Fact]
        public void UpdateProductoOk()
        {
            //Arrange
            Fixture fixture = new Fixture();
            var productos = fixture.Create<List<ProductoRequestDto>>();
            //Act
            var okResult = _controller.UpdateProducto(productos);
            //Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public async Task UpdateProductoException()
        {
            //Arrange
            var mockServicio = new Mock<IFacOpheliaService>();
            Fixture fixture = new Fixture();
            var productos = fixture.Create<List<ProductoRequestDto>>();

            mockServicio.Setup(m => m.UpdateProductos(It.IsAny<List<ProductoRequestDto>>())).Throws(new Exception());
            _controller = new FacOpheliaController(mockServicio.Object);

            //Act
            var okResult = await _controller.UpdateProducto(productos);

            //Assert
            Assert.IsType<BadRequestObjectResult>(okResult);
        }
    }
}
