using AutoMapper;
using EntityFrameworkCore.Testing.Moq.Helpers;
using FacOphelia.Data;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace FacOphelia.UnitTest.MockDbContext
{
    public class AppDbContextMock
    {
        protected AppDbContext ConstruirContext(string nombreDB)
        {
            var opciones = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(nombreDB)
                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                .EnableSensitiveDataLogging(true)
                .Options;

            var dbContextToMock = new AppDbContext(opciones);
            var mockedDbContext = new MockedDbContextBuilder<AppDbContext>()
                .UseDbContext(dbContextToMock)
                .UseConstructorWithParameters(opciones)
                .MockedDbContext;

            return mockedDbContext;
        }
    }
}