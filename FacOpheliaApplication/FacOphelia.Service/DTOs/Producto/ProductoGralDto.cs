﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacOphelia.Service.DTOs
{
    public abstract class ProductoGralDto
    {
        public decimal IdProducto { set; get; }
        public string Nombre { get; set; }
    }
}
