﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacOphelia.Service.DTOs
{
    public class ProductoRequestDto : ProductoGralDto
    {
        public decimal? Precio { set; get; }
        public decimal? Inventario { set; get; }
        public decimal? IdClase { set; get; }
    }
}
