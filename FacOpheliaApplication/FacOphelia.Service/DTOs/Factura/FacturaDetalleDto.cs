﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacOphelia.Service.DTOs
{
   public class FacturaDetalleDto
    {
        public decimal Consecutivo { get; set; }
        public string Producto { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Precio { set; get; }
        public decimal Subtotal { set; get; }
    }
}
