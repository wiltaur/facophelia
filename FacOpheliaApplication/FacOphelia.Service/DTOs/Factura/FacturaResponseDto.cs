﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacOphelia.Service.DTOs
{
    public class FacturaResponseDto: FacturaResponseGralDto
    {
        public DateTime Fecha { get; set; }
        public string NombreCliente { get; set; }
        public string FormaPago { set; get; }
        public ICollection<FacturaDetalleDto> Detalle { set; get; }
        public decimal Total { set; get; }
    }
}
