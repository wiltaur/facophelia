﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacOphelia.Service.DTOs
{
    public class FacturaDetRequestDto
    {
        public decimal IdProducto { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Precio { get; set; }
    }
}
