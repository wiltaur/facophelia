﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacOphelia.Service.DTOs
{
    public class FacturaRequestDto
    {
        public string IdCliente { get; set; }
        public decimal IdPago { set; get; }
        public ICollection<FacturaDetRequestDto> Detalle { set; get; }
    }
}
