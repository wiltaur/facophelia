﻿using FacOphelia.Data.Models;
using FacOphelia.Service.DTOs;
using FacOphelia.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacOphelia.Service.Servicios
{
    public class FacOpheliaService : IFacOpheliaService
    {
        private readonly IFacOpheliaRepository _facOpheliaRepository;
        private readonly IBaseRepository<Factura> _repFactura;
        private readonly IBaseRepository<Producto> _repProducto;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="facOpheliaRepository"></param>
        public FacOpheliaService(IBaseRepository<Factura> repFactura, IBaseRepository<Producto> repProducto, IFacOpheliaRepository facOpheliaRepository)
        {
            _facOpheliaRepository = facOpheliaRepository;
            _repFactura = repFactura;
            _repProducto = repProducto;
        }

        /// <summary>
        /// Retorna una lista de Facturas o una Factura si está filtrada por Número de Factura
        /// </summary>
        /// <param name="numberTicket"></param>
        /// <returns></returns>
        public async Task<List<FacturaResponseDto>> GetFacturas(decimal? numFactura)
        {
            return await _facOpheliaRepository.GetFacturas(numFactura);
        }

        /// <summary>
        /// Define la logica para la insercion de la Factura
        /// </summary>
        /// <param name="material"></param>
        /// <returns></returns>
        public async Task<FacturaCreaResponseDto> AddFactura(FacturaRequestDto factura)
        {
            decimal detalleConsecutivo = 1;
            var response = new FacturaCreaResponseDto();
            
            var facturaGuardar = new Factura();
            facturaGuardar.Fecha = DateTime.Now;
            facturaGuardar.IdCliente = factura.IdCliente;
            facturaGuardar.IdPago = factura.IdPago;
            facturaGuardar.FacturaDetalles = (from f in factura.Detalle
                                     select new FacturaDetalle
                                     {
                                         NumFacDetalle = detalleConsecutivo++,
                                         IdProducto = f.IdProducto,
                                         Cantidad = f.Cantidad,
                                         PrecioVenta = f.Precio
                                     }).ToList(); 
              
            await _repFactura.Add(facturaGuardar);
            response.NumFactura = await _facOpheliaRepository.GetLastNumFactura();
            return response;
        }

        public async Task<FacturaEliminaResponseDto> DeleteFactura( decimal numFactura)
        {
            var response = new FacturaEliminaResponseDto();

            Factura factura = _repFactura.GetById(numFactura).Result;
            await _repFactura.Delete(factura);
            response.NumFactura = numFactura;
            return response;
        }

        public async Task<List<ProductoResponseDto>> UpdateProductos(ICollection<ProductoRequestDto> productos)
        {
            List<ProductoResponseDto> response = new List<ProductoResponseDto>();

            foreach (var producto in productos)
            {
                Producto prodToSave =  _repProducto.GetById(producto.IdProducto).Result;
                prodToSave = prodToSave == null ? new Producto() : prodToSave;
                prodToSave.IdClase = producto.IdClase.HasValue ? producto.IdClase.Value : prodToSave.IdClase;
                prodToSave.Nombre = !string.IsNullOrEmpty(producto.Nombre) ? producto.Nombre : prodToSave.Nombre;
                prodToSave.Precio = producto.Precio.HasValue ? producto.Precio.Value : prodToSave.Precio;
                prodToSave.Inventario = producto.Inventario.HasValue ? producto.Inventario.Value : prodToSave.Inventario;
                await _repProducto.Update(prodToSave);
                response.Add(new ProductoResponseDto { IdProducto = prodToSave.IdProducto, Nombre = prodToSave.Nombre });
            }

            return response;
        }
    }
}