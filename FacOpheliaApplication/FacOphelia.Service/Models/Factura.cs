﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FacOphelia.Data.Models
{
    [Table("Factura")]
    public partial class Factura
    {
        public Factura()
        {
            FacturaDetalles = new HashSet<FacturaDetalle>();
        }

        [Key]
        [Column(TypeName = "numeric(18, 0)")]
        public decimal NumFactura { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Fecha { get; set; }
        [Required]
        [StringLength(20)]
        public string IdCliente { get; set; }
        [Column(TypeName = "numeric(1, 0)")]
        public decimal IdPago { get; set; }

        [ForeignKey(nameof(IdCliente))]
        [InverseProperty(nameof(Cliente.Facturas))]
        public virtual Cliente IdClienteNavigation { get; set; }
        [ForeignKey(nameof(IdPago))]
        [InverseProperty(nameof(TipoPago.Facturas))]
        public virtual TipoPago IdPagoNavigation { get; set; }
        [InverseProperty(nameof(FacturaDetalle.NumFacturaNavigation))]
        public virtual ICollection<FacturaDetalle> FacturaDetalles { get; set; }
    }
}
