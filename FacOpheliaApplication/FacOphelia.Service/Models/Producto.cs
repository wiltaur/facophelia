﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FacOphelia.Data.Models
{
    [Table("Producto")]
    public partial class Producto
    {
        public Producto()
        {
            FacturaDetalles = new HashSet<FacturaDetalle>();
        }

        [Key]
        [Column(TypeName = "numeric(4, 0)")]
        public decimal IdProducto { get; set; }
        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }
        [Column(TypeName = "numeric(10, 2)")]
        public decimal Precio { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public decimal Inventario { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal IdClase { get; set; }

        [ForeignKey(nameof(IdClase))]
        [InverseProperty(nameof(ProductoClase.Productos))]
        public virtual ProductoClase IdClaseNavigation { get; set; }
        [InverseProperty(nameof(FacturaDetalle.IdProductoNavigation))]
        public virtual ICollection<FacturaDetalle> FacturaDetalles { get; set; }
    }
}
