﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FacOphelia.Data.Models
{
    [Table("TipoPago")]
    public partial class TipoPago
    {
        public TipoPago()
        {
            Facturas = new HashSet<Factura>();
        }

        [Key]
        [Column(TypeName = "numeric(1, 0)")]
        public decimal IdPago { get; set; }
        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }
        [StringLength(50)]
        public string MasDetalle { get; set; }

        [InverseProperty(nameof(Factura.IdPagoNavigation))]
        public virtual ICollection<Factura> Facturas { get; set; }
    }
}
