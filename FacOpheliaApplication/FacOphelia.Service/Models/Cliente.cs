﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FacOphelia.Data.Models
{
    [Table("Cliente")]
    public partial class Cliente
    {
        public Cliente()
        {
            Facturas = new HashSet<Factura>();
        }

        [Key]
        [StringLength(20)]
        public string IdCliente { get; set; }
        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }
        [Required]
        [StringLength(50)]
        public string Apellido { get; set; }
        [StringLength(50)]
        public string Direccion { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaNacimiento { get; set; }
        [Column(TypeName = "numeric(10, 0)")]
        public decimal? Telefono { get; set; }
        [StringLength(50)]
        public string Email { get; set; }

        [InverseProperty(nameof(Factura.IdClienteNavigation))]
        public virtual ICollection<Factura> Facturas { get; set; }
    }
}
