﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FacOphelia.Data.Models
{
    [Table("FacturaDetalle")]
    [Index(nameof(IdProducto), nameof(NumFactura), Name = "IX_FacturaDetalle", IsUnique = true)]
    public partial class FacturaDetalle
    {
        [Key]
        [Column(TypeName = "numeric(4, 0)")]
        public decimal NumFacDetalle { get; set; }
        [Key]
        [Column(TypeName = "numeric(18, 0)")]
        public decimal NumFactura { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal Cantidad { get; set; }
        [Column(TypeName = "numeric(10, 2)")]
        public decimal PrecioVenta { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal IdProducto { get; set; }

        [ForeignKey(nameof(IdProducto))]
        [InverseProperty(nameof(Producto.FacturaDetalles))]
        public virtual Producto IdProductoNavigation { get; set; }
        [ForeignKey(nameof(NumFactura))]
        [InverseProperty(nameof(Factura.FacturaDetalles))]
        public virtual Factura NumFacturaNavigation { get; set; }
    }
}
