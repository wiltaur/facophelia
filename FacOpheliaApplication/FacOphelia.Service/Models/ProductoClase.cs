﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FacOphelia.Data.Models
{
    [Table("ProductoClase")]
    public partial class ProductoClase
    {
        public ProductoClase()
        {
            Productos = new HashSet<Producto>();
        }

        [Key]
        [Column(TypeName = "numeric(4, 0)")]
        public decimal IdClase { get; set; }
        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }
        [StringLength(500)]
        public string Descripcion { get; set; }

        [InverseProperty(nameof(Producto.IdClaseNavigation))]
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
