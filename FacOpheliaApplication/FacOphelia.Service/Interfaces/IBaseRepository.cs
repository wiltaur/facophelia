﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FacOphelia.Service.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        Task<T> GetById(decimal id);
        Task Add(T entity);
        Task AddRange(ICollection<T> entity);
        Task Update(T entity);
        Task Delete(T entity);

    }
}
