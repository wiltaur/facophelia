﻿using FacOphelia.Service.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FacOphelia.Service.Interfaces
{
    public interface IFacOpheliaRepository
    {
        Task<List<FacturaResponseDto>> GetFacturas(decimal? idFactura);
        Task<decimal> GetLastNumFactura();

    }
}