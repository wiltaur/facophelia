﻿using FacOphelia.Service.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FacOphelia.Service.Interfaces
{
    public interface IFacOpheliaService
    {
        Task<List<FacturaResponseDto>> GetFacturas(decimal? numFactura);
        Task<FacturaCreaResponseDto> AddFactura(FacturaRequestDto factura);
        Task<FacturaEliminaResponseDto> DeleteFactura(decimal numFactura);
        Task<List<ProductoResponseDto>> UpdateProductos(ICollection<ProductoRequestDto> productos);

    }
}